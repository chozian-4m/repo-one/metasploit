# Metasploit 5.0.84

## Introduction
Metasploit is an exploitation and vulnerability validation tool that helps you divide the penetration testing workflow into smaller and more manageable tasks. With Metasploit Pro, you can leverage the power of the Metasploit Framework and its exploit database through a web based user interface to perform security assessments and vulnerability validation.

## Running The Container
In it's most basic form, the image can be run locally by executing the following:

`docker run -it metasploit5.0.79`

This will launch the Metasploit CLI after creating a temporary database instance.

## Licensing
The license for Metasploit can be found in `/licenses` inside the container.
