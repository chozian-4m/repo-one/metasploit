ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}


ENV VENDOR=rapid7 \
    PACKAGE_NAME=metasploit-framework.x86_64.rpm

COPY ${PACKAGE_NAME} /tmp/${PACKAGE_NAME}
COPY LICENSE /licenses/LICENSE

RUN dnf -y update && dnf -y upgrade \
    && dnf -y localinstall --nogpgcheck /tmp/${PACKAGE_NAME} \
    && useradd metasploit \
    && echo CtrlAltDelBurstAction=none >> /etc/systemd/system.conf \
    && rm /tmp/${PACKAGE_NAME}

USER metasploit:metasploit

HEALTHCHECK --timeout=15s CMD ["msfconsole", "-v"]

CMD msfdb init --use-defaults && msfconsole
